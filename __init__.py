#!/usr/bin/env python3

from PySide2.QtGui import QIcon
from PySide2.QtWidgets import QApplication, QSystemTrayIcon, QMenu, QAction
from gi.repository import PackageKitGlib, Gio, GLib
import time, re, signal, threading, sys

class Application(QApplication): # override application to force pyside to bump signals from c++ to python
	def event(self, e):
		return QApplication.event(self, e)

class PkRunner:
	def __init__(self):
		self.pk=PackageKitGlib.Client()
		self.queue=[]
		self.current_task=None
		self.returncode="success"
		self.updates=tuple()
		self.done=True
		self.tray=None
		self.cbutton=None
		self.apply=None
		self.app=None
		self.exit=False
		self.lastact=time.time()
		self.cur_tooltip=""
		self.cur_progress=-1
		self.cur_item=""
		self.cur_item_progress=-1
		self.canc=None
		self.request_canc=False

	def setui(self,app,tray,apply,cbutton):
		self.app=app
		self.tray=tray
		self.apply=apply
		self.cbutton=cbutton

	def updateProgress(self,progress):
		self.cur_progress=int(progress)
		self.updateTooltip()

	def updateItemProgress(self,progress):
		self.cur_item_progress=int(progress)
		self.updateTooltip()

	def updateTooltip(self):
		self.tray.setToolTip(self.cur_tooltip+(" "+str(self.cur_progress)+"%" if self.cur_progress>0 else "")+("\n"+self.cur_item+(" "+str(self.cur_item_progress)+"%" if self.cur_item_progress>0 else "") if self.cur_item!="" else ""))

	def setChecking(self):
		self.tray.setIcon(QIcon.fromTheme("update-none"))
		self.tray.setVisible(True)
		self.cur_tooltip="Checking for updates..."
		self.cur_item=""
		self.cur_progress=-1
		self.cur_item_progress=-1
		self.updateTooltip()

	def setNone(self):
		self.tray.setIcon(QIcon.fromTheme("update-none"))
		self.tray.setVisible(False)
		self.cur_tooltip=("No Updates")
		self.cur_item=""
		self.cur_progress=-1
		self.cur_item_progress=-1
		self.updateTooltip()

	def setInstalling(self,package=""):
		self.tray.setIcon(QIcon.fromTheme("install"))
		self.tray.setVisible(True)
		self.cur_tooltip=("Installing updates...")
		self.cur_item=package
		self.cur_progress=-1
		self.cur_item_progress=-1
		self.updateTooltip()

	def setError(self,error):
		self.tray.setIcon(QIcon.fromTheme("error"))
		self.tray.setVisible(True)
		self.cur_tooltip="Unable to fetch updates:\n"+(error.value_nick if type(error) != str else error)
		print(self.cur_tooltip)
		self.cur_item=""
		self.cur_progress=-1
		self.cur_item_progress=-1
		self.updateTooltip()

	def pk_progress(self,progress, progress_type,*args):
		self.lastact=time.time()
		val=None
		if progress_type is PackageKitGlib.ProgressType.PACKAGE_ID:
			val=progress.get_package_id()
		elif progress_type is PackageKitGlib.ProgressType.PACKAGE:
			if self.current_task[1]=="update_packages_async":
				self.setInstalling(progress.get_package().get_name())
		elif progress_type is PackageKitGlib.ProgressType.TRANSACTION_ID:
			val=progress.get_transaction_id()
		elif progress_type is PackageKitGlib.ProgressType.PERCENTAGE:
			val=progress.get_percentage()
			self.updateProgress(val)
		elif progress_type is PackageKitGlib.ProgressType.ITEM_PROGRESS:
			val=progress.get_item_progress().get_percentage()
			self.updateItemProgress(val)
		elif progress_type is PackageKitGlib.ProgressType.ALLOW_CANCEL:
			val=progress.get_allow_cancel()
		elif progress_type is PackageKitGlib.ProgressType.STATUS:
			val=progress.get_status().value_nick
			if val=="cancel":
				print("Cancelling operation, please wait...")
		elif progress_type is PackageKitGlib.ProgressType.ROLE:
			val=progress.get_role().value_nick
		elif progress_type is PackageKitGlib.ProgressType.CALLER_ACTIVE:
			val=progress.get_caller_active()
		elif progress_type is PackageKitGlib.ProgressType.ELAPSED_TIME:
			val=progress.get_elapsed_time()
		elif progress_type is PackageKitGlib.ProgressType.REMAINING_TIME:
			val=progress.get_remaining_time()
		elif progress_type is PackageKitGlib.ProgressType.SPEED:
			val=progress.get_speed()
		elif progress_type is PackageKitGlib.ProgressType.DOWNLOAD_SIZE_REMAINING:
			val=progress.get_download_size_remaining()
		elif progress_type is PackageKitGlib.ProgressType.UID:
			val=progress.get_uid()
		elif progress_type is PackageKitGlib.ProgressType.TRANSACTION_FLAGS:
			val=progress.get_transaction_flags()
		elif progress_type is PackageKitGlib.ProgressType.INVALID:
			val=progress.get_invalid()
		if val is not None:
			print(progress_type.value_nick+": "+str(val))
		return

	def pk_complete(self,client,async_result,*args):
		self.lastact=time.time()
		self.done=True
		try:
			result=client.generic_finish(async_result)
			self.returncode=result.get_exit_code().value_nick
			error=result.get_error_code()
			if error is not None:
				error=error.get_code()
		except GLib.GError as e:
			result=e
			self.returncode=e.code
			error=e.message
			pass

		if self.returncode!="success":
			if self.canc is not None and self.canc.is_cancelled():
				errorcode="Cancelled"
			elif error is None:
				errorcode="Unknown error"
			else:
				errorcode=error
			self.setError(errorcode)

		if self.current_task[1]=="get_updates_async":
			self.updates=result.get_package_array()
			self.setNumber(len(self.updates))
			for x in self.updates:
				print(x.get_info().value_nick,x.get_id(),x.get_summary())

		return

	def setNumber(self,count):
		if count>0:
			self.apply.setDisabled(False)
			hasImportant=len(tuple(x for x in self.updates if x.get_info() in [PackageKitGlib.InfoEnum.IMPORTANT]))>0
			hasSecurity=len(tuple(x for x in self.updates if x.get_info() in [PackageKitGlib.InfoEnum.SECURITY]))>0
			hasMedium=len(tuple(x for x in self.updates if x.get_info() in [PackageKitGlib.InfoEnum.NORMAL,PackageKitGlib.InfoEnum.ENHANCEMENT]))>0
			if hasImportant or hasSecurity:
				self.tray.setIcon(QIcon.fromTheme("update-high"))
			elif hasMedium:
				self.tray.setIcon(QIcon.fromTheme("update-medium"))
			else:
				self.tray.setIcon(QIcon.fromTheme("update-low"))
			self.tray.setVisible(True)
			self.tray.setToolTip(str(count)+" updates available"+("\nSecurity updates available" if hasSecurity else "")+("\nImportant updates available" if hasImportant else "")+("" if hasSecurity or hasImportant or hasMedium else "\nLow priority updates only"))
		else:
			self.setNone()

	def finish(self):
		global cancel
		self.queue=[]
		self.done=True
		self.current_task=None
		self.returncode="success"
		self.request_canc=False

	def nextStep(self):
		if len(self.updates)==0:
			self.checkUpdates()
		else:
			self.applyUpdates()

	def checkUpdates(self):
		self.queue.append([False,self.setChecking])
		self.queue.append([True,"refresh_cache_async"])
		self.queue.append([True,"get_updates_async"])

	def applyUpdates(self):
		self.queue.append([False,self.setInstalling])
		self.queue.append([True,"update_packages_async"])
		self.queue.append([True,"refresh_cache_async"])
		self.queue.append([True,"get_updates_async"])

	def procQueue(self):
		global cancel
		if self.request_canc and self.canc is not None:
			self.perform_cancel()
		if not self.done:
			self.lastact=time.time()
			return
		if self.canc is not None:
			Gio.Cancellable.pop_current(self.canc)
			self.canc=None
		if self.current_task is not None and (self.returncode!="success" or len(self.queue)==0):
			self.lastact=time.time()
			self.finish()
		elif len(self.queue)>0:
			self.lastact=time.time()
			op=self.queue.pop(0)
			self.current_task=op
			if op[0]:
				self.done=False
				self.canc=Gio.Cancellable()
				Gio.Cancellable.push_current(self.canc)
				# this approach is necessary since each method has different expected args
				if op[1]=="refresh_cache_async":
					self.pk.refresh_cache_async(True,self.canc,self.pk_progress,tuple(),self.pk_complete,tuple())
				elif op[1]=="get_updates_async":
					self.pk.get_updates_async(PackageKitGlib.FilterEnum.NONE,self.canc,self.pk_progress,tuple(),self.pk_complete,tuple())
				elif op[1]=="update_packages_async":
					self.pk.update_packages_async(PackageKitGlib.TransactionFlagEnum.NONE,tuple(x.get_id() for x in self.updates),self.canc,self.pk_progress,tuple(),self.pk_complete,tuple())
				else:
					raise Exception("Unknown PK operation: "+op[1])
			else:
				self.done=False
				op[1]()
				self.done=True

	def runloop(self):
		while not self.exit or self.current_task is not None:
			self.procQueue()
			if self.done and self.current_task is None and self.lastact<time.time()-300:
				self.lastact=time.time()
				self.checkUpdates()
			time.sleep(0.001) # yield to other thread
		app.quit()

	def exitprogram(self):
		self.perform_cancel()
		self.exit=True

	def perform_cancel(self):
		if self.canc is not None:
			self.canc.cancel()
			self.request_canc=False

	def request_cancel(self):
		if self.current_task is not None:
			self.request_canc=True

def exitprogram(*args):
	global app
	global pkr
	pkr.exitprogram()
signal.signal(signal.SIGINT,exitprogram)
signal.signal(signal.SIGHUP,exitprogram)
signal.signal(signal.SIGTERM,exitprogram)
signal.signal(signal.SIGQUIT,exitprogram)
signal.signal(signal.SIGABRT,exitprogram)

pkr=PkRunner()

app = Application(sys.argv)
app.setQuitOnLastWindowClosed(False)

# Create the tray
tray = QSystemTrayIcon()
tray.setIcon(QIcon.fromTheme("update-none"))
tray.setVisible(True)
tray.setToolTip("PackageKit Updater not running,\n please check for updates manually")
tray.activated.connect(pkr.nextStep)

# Create the menu
menu = QMenu()
check = QAction("Check for Updates")
check.triggered.connect(pkr.checkUpdates)
menu.addAction(check)

apply = QAction("Apply Updates")
apply.triggered.connect(pkr.applyUpdates)
apply.setDisabled(True)
menu.addAction(apply)

cancel = QAction("Cancel")
cancel.triggered.connect(pkr.request_cancel)
menu.addAction(cancel)

# Add a Quit option to the menu.
quit = QAction("Quit")
quit.triggered.connect(exitprogram)
menu.addAction(quit)

# Add the menu to the tray
tray.setContextMenu(menu)

pkr.setui(app,tray,apply,cancel)

t=threading.Thread(None,pkr.runloop)
t.start()

pkr.checkUpdates()

app.startTimer(200) # regularly update the python event loop so signals are handled
sys.exit(app.exec_())
t.join()
