PackageKit Updater
==================

A small Qt application intended to be run on login. Automatically checks packagekit for updates every 5 minutes and displays a tray icon when any are present. Clicking the icon (or opening the menu and selecting apply) applies the updates. Supports cancelling manually as well as when the workstation is being shut down.

Automatic updates with minimal clicks. Does not require plasma-discover or any other specific frontend.
